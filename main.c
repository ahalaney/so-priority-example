/**
 * A short UDP packet sender for testing SO_PRIORITY usage.
 */
#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char **argv) {
    char *address = NULL;
    int port = 0;
    int prio = -1;
    int c;
    int sockfd;
    struct sockaddr_in sockaddr = {0};
    int size;

    while ((c = getopt(argc, argv, "c:p:q:h")) != -1) {
        switch (c) {
            case 'c':
                address = optarg;
                break;
            case 'p':
                port = atoi(optarg);
                break;
            case 'q':
                prio = atoi(optarg);
                break;
            case 'h':
                printf("Example usage:\n");
                printf("%s -c 8.8.8.8 -p 30000 -q 3\n", argv[0]);
                printf("client UDP to 8.8.8.8, port 30000, with SO_PRIORITY 3\n");
                break;
            case '?':
                printf("Failed to parse options\n");
                printf("Unknown option: %c\n", optopt);
                return -1;
            default:
                printf("?? getopt returned character code 0%d ??\n", c);
                return -1;
            }
      }

    if (!address || !port) {
        printf("address and port are mandatory\n");
        return -1;
    }

    /* Make our socket */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        printf("Failed to create sockfd: %d\n", errno);
        return errno;
    }

    /* Set SO_PRIORITY if asked to do so */
    if (prio >= 0) {
        if (setsockopt(sockfd, SOL_SOCKET, SO_PRIORITY, &prio, sizeof(prio))) {
            printf("Failed to set SO_PRIORITY: %d\n", errno);
            return errno;
        }
    }

    /* send a packet */
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(port);
    char *data = "hello world";
    inet_pton(AF_INET, address, &(sockaddr.sin_addr));
    size = sendto(sockfd, data, strlen(data), 0, (const struct sockaddr *)&sockaddr, sizeof(sockaddr));
    if (size < 0) {
        printf("Failed to sendto(): %d\n", errno);
        return errno;
    }
    else {
        printf("Sent packet of size: %d\n", size);
    }

    close(sockfd);
    return 0;
}
